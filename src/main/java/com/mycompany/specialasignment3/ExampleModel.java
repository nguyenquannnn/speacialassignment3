/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.specialasignment3;

import java.io.IOException;
import java.io.Serializable;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

/**
 *  Backing bean for the index.xhtml view
 * 
 * @author 1632647
 */
@Named("exampleModel")
@SessionScoped
public class ExampleModel implements Serializable {
    //Binding field to the text input
    private String input;
    Pattern codeUSVar1;
    Pattern codeUSVar2;
    Pattern codeCaVar1;
    Pattern codeCaVar2;

    private String output;
    
    //Initialize stuff
    @PostConstruct
    public void init() {
        codeUSVar1 = Pattern.compile("[0-9]{5}");
        codeUSVar2 = Pattern.compile("[0-9]{5}-[0-9]{4}");
        codeCaVar1 = Pattern.compile("[a-zA-Z][0-9][a-zA-Z][-\\s][0-9][a-zA-Z][0-9]");
        codeCaVar2 = Pattern.compile("[a-zA-Z][0-9][a-zA-Z][0-9][a-zA-Z][0-9]");
    }
    
    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    /**
     * Methods that determine if the input matches with which type of format
     * Redirect after identification
     *
     * @throws IOException
     */
    public void validate() throws IOException {
        if(input.matches(codeUSVar1.pattern())) {
            output = "Zip Code";
            redirect();
        } else if(input.matches(codeUSVar2.pattern())) {
            output = "Zip Code + Extension";
            redirect();
        } else if(input.matches(codeCaVar1.pattern())) {
            output = "Postal Code";
            redirect();
        } else if(input.matches(codeCaVar2.pattern())) {
            output = "Postal Code";
            redirect();
        }
    }

    /**
     * Private helper method that helps with the redirection using FacesContext
     * @throws IOException
     */
    private void redirect() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect("output.xhtml");
    }
}
