package com.mycompany.specialasignment3;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Pattern;

/**
 * Custom validator for US zip code and Ca postal Code
 */
@FacesValidator("codeValidator")
public class CodeValidator implements Validator {
    Pattern codeUSVar1;
    Pattern codeUSVar2;
    Pattern codeCaVar1;
    Pattern codeCaVar2;

    /**
     * Method that is needed to be overloaded inorder to validate input.
     * Use serveral regex pattern to check for string format.
     * Ìf constraint are not met throw an exception including the message to be display.
     *
     * @param facesContext
     * @param uiComponent
     * @param o
     * @throws ValidatorException
     */
    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object o)
            throws ValidatorException {
        codeUSVar1 = Pattern.compile("[0-9]{5}");
        codeUSVar2 = Pattern.compile("[0-9]{5}-[0-9]{4}");
        codeCaVar1 = Pattern.compile("[a-zA-Z][0-9][a-zA-Z][-\\s][0-9][a-zA-Z][0-9]");
        codeCaVar2 = Pattern.compile("[a-zA-Z][0-9][a-zA-Z][0-9][a-zA-Z][0-9]");
        if(o == null) {
            throw new ValidatorException(new FacesMessage("Code can't be empty"));
        } else {
            String input = (String) o.toString();
            if(input.matches(codeUSVar1.pattern())) {
                return;
            } else if(input.matches(codeUSVar2.pattern())) {
                return;
            } else if(input.matches(codeCaVar1.pattern())) {
                return;
            } else if(input.matches(codeCaVar2.pattern())) {
                return;
            } else {
                throw new ValidatorException(new FacesMessage("Code does not match the format of US's Zip code or Canada's postal code."));
            }
        }
    }
}
